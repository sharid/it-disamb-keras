	


import codecs

#goldfile = codecs.open("/home/staff7/sharid/miniconda2/envs/smt-pronouns/test_data/EventPronsData_itLABELS_sharedTaskFormat.test", "r",  encoding = "utf-8")
goldfile = codecs.open("/home/staff7/sharid/miniconda2/envs/smt-pronouns/test_data/hypothesis/errors_gold", "r",  encoding = "utf-8")
predictedfile = codecs.open("/home/staff7/sharid/miniconda2/envs/smt-pronouns/test_data/hypothesis/errors_pred", "r",  encoding = "utf-8")


line_predicted = predictedfile.readline()

#counters
an_as_an,an_as_ev,an_as_pl = 0.0, 0.0, 0.0
ev_as_an,ev_as_ev,ev_as_pl = 0.0, 0.0, 0.0 
pl_as_an,pl_as_ev,pl_as_pl = 0.0, 0.0, 0.0


for line in goldfile:
	gold_elements = line.split("\t")
	pred_elements = line_predicted.split("\t")
	gold = gold_elements[0].lower()
	pred = pred_elements[0].lower()
	#pred = pred_elements[2].lower() #maxENT
	if pred == "it_anaphoric" and gold == "it_anaphoric":
		an_as_an += 1
	if pred == "it_anaphoric" and gold == "it_event":
		an_as_ev += 1
	if pred == "it_anaphoric" and gold == "it_pleonastic":
		an_as_pl += 1
	if pred == "it_event" and gold == "it_anaphoric":
		ev_as_an += 1
	if pred == "it_event" and gold == "it_event":
		ev_as_ev += 1
	if pred == "it_event" and gold == "it_pleonastic":
		ev_as_pl += 1
	if pred == "it_pleonastic" and gold == "it_anaphoric":
		pl_as_an += 1
	if pred == "it_pleonastic" and gold == "it_event":
		pl_as_ev += 1
	if pred == "it_pleonastic" and gold == "it_pleonastic":
		pl_as_pl += 1
	
	#read again
	line_predicted = predictedfile.readline()

print "an_as_an", "an_as_ev", "an_as_pl", "-->", int(an_as_an), int(an_as_ev), int(an_as_pl)
print "ev_as_an", "ev_as_ev", "ev_as_pl", "-->", int(ev_as_an), int(ev_as_ev), int(ev_as_pl)
print "pl_as_an", "pl_as_ev", "pl_as_pl",  "-->", int(pl_as_an), int(pl_as_ev), int(pl_as_pl)

total = an_as_an + an_as_ev + an_as_pl + ev_as_an + ev_as_ev + ev_as_pl + pl_as_an + pl_as_ev + pl_as_pl

'''
anaphoric_precision = an_as_an/(an_as_an + an_as_ev + an_as_pl)
event_precision = ev_as_ev/(ev_as_ev + ev_as_an + ev_as_pl)
pleonastic_precision = pl_as_pl/(pl_as_pl + pl_as_ev + pl_as_an)

anaphoric_recall = an_as_an/(an_as_an + ev_as_an + pl_as_an)
event_recall = ev_as_ev/(ev_as_ev + an_as_ev + pl_as_ev)
pleonastic_recall = pl_as_pl/(pl_as_pl + an_as_pl + ev_as_pl)

anaphoric_f1 = 2*((anaphoric_precision * anaphoric_recall)/(anaphoric_precision + anaphoric_recall))
event_f1 = 2*((event_precision * event_recall)/(event_precision + event_recall))
pleonastic_f1 = 2*((pleonastic_precision * pleonastic_recall)/(pleonastic_precision + pleonastic_recall))
'''
accuracy = (an_as_an + ev_as_ev + pl_as_pl) / total
'''
print ("anaphoric_precision", anaphoric_precision)
print ("anaphoric_recall", anaphoric_recall)
print ("anaphoric_f1", anaphoric_f1)
print ("\n")
print ("pleonastic_precision", pleonastic_precision) 
print ("pleonastic_recall", pleonastic_recall)
print ("pleonastic_f1", pleonastic_f1)
print ("\n")
print ("event_precision", event_precision) 
print ("event_recall", event_recall) 
print ("event_f1", event_f1)
'''
print ("\n")
print "accuracy:", accuracy, "(", (an_as_an + ev_as_ev + pl_as_pl), "/", total, ")"




goldfile.close()
predictedfile.close()












