<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.

# README #

It-disambiguation system working on annotated data. 

System used in the paper http://aclweb.org/anthology/D/D17/D17-1137.pdf

Code based on https://github.com/TurkuNLP/smt-pronouns

### What is this repository for? ###

It-disambiguation system

### How do I get set up? ###
This is how I ran it for training:

THEANO_FLAGS="device=gpu0",floatX=float32 python rnn_stack_simple_it2.py ./train_data/file.train ./dev_data/file.dev n temp_outputprefix stack &

For predicting:

python output_set2.py  model.json weights_file train_data_file test_file outputprefix &

### Models & weights used in the 2017 paper ###

Gold (RNN-gold)

gold.json
gold_stack_True_48_MR_0.471922212991.hdf5

Noisy (RNN-silver) 

noisy.json
noisy_stack_True_4_MR_0.756018518519.hdf5

Gold+Noisy (RNN-combined)

goldANDnoisy.json
goldANDnoisy_stack_True_4_MR_0.582238794161.hdf5



###Training data format####

Basically the shared task format with pronoun classes replaced and an additional column for the features from the MaxEnt.

it-class \t \t source \t target \t alignment \t docID \t feature_vector_from_MaxEnt


NOTE: Data folder contains gold data only 


### Who do I talk to? ###

* sharid.loaiciga@gmail.com
