%\title{emnlp 2017 instructions}
% File emnlp2017.tex
%

\documentclass[11pt,letterpaper]{article}
\usepackage{emnlp2017}
\usepackage{times}
\usepackage{latexsym}


%our packages
\usepackage{color}
\usepackage{url}
\usepackage{graphicx} 
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\usepackage[inline]{enumitem}
\newcommand\ngram{\ensuremath{n}-gram}
\usepackage{linguex}


% Uncomment this line for the final submission:
\emnlpfinalcopy

%  Enter the EMNLP Paper ID here:
\def\emnlppaperid{***}

% To expand the titlebox for more authors, uncomment
% below and set accordingly.
% \addtolength\titlebox{.5in}    

\newcommand\BibTeX{B{\sc ib}\TeX}

\title{What is it? Disambiguating the different readings of the pronoun `it'}

% Author information can be set in various styles:
% For several authors from the same institution:
% \author{Author 1 \and ... \and Author n \\
%         Address line \\ ... \\ Address line}
% if the names do not fit well on one line use
%         Author 1 \\ {\bf Author 2} \\ ... \\ {\bf Author n} \\
% For authors from different institutions:
\author{Sharid Lo\'{a}iciga \\ \normalsize{Uppsala University} \\\normalsize{Dept. of Linguistics \& Philology} \\
\normalsize{Uppsala, Sweden}\\
\normalsize{sharid.loaiciga@lingfil.uu.se}
\And Liane Guillou \\ \normalsize{University of Edinburgh} \\ \normalsize{School of Informatics} \\
\normalsize{Scotland, United Kingdom}\\
\normalsize{lguillou@inf.ed.ac.uk}\\
\And Christian Hardmeier \\ \normalsize{Uppsala University} \\ \normalsize{Dept. of Linguistics \& Philology} \\
\normalsize{Uppsala, Sweden}\\
\normalsize{christian.hardmeier@lingfil.uu.se}}
%         \And  ... \And
%         Author n \\ Address line \\ ... \\ Address line}
% To start a seperate ``row'' of authors use \AND, as in
% \author{Author 1 \\ Address line \\  ... \\ Address line
%         \AND
%         Author 2 \\ Address line \\ ... \\ Address line \And
%         Author 3 \\ Address line \\ ... \\ Address line}
% If the title and author information does not fit in the area allocated,
% place \setlength\titlebox{<new height>} right after
% at the top, where <new height> can be something larger than 2.25in

\date{}

\begin{document}

\maketitle

\begin{abstract}
In this paper, we address the problem of predicting one of three functions for the English pronoun `it': anaphoric, event reference or pleonastic. This disambiguation is valuable in the context of machine translation and coreference resolution. We present experiments using a \textsc{MaxEnt} classifier trained on gold-standard data and self-training experiments of an \textsc{Rnn} trained on silver-standard data, annotated using the \textsc{MaxEnt} classifier. Lastly, we report on an analysis of the strengths of these two models.

\end{abstract}



\section{Introduction}

We address the problem of disambiguating the English pronoun `it', which may function as a pleonastic, anaphoric, or event reference pronoun. As an \textit{anaphoric} pronoun, `it' corefers with a noun phrase (called the \textit{antecedent}), as in example (1):

\ex. I have a \underline{bicycle}. \underline{It} is red.

%`\textit{I have a \underline{bicycle}. \underline{It} is red.}'.

\textit{Pleonastic} pronouns, in contrast, do not refer to anything but are required to fill the subject position in many languages, including English, French and German:

\ex. \underline{It} is raining.

%, for instance `\textit{\underline{It} is raining}'.

\textit{Event reference} pronouns are anaphoric, but instead of referring to a noun phrase, they refer to a verb, verb phrase, clause or even an entire sentence, as in example (3):
%`\textit{He lost his job. \underline{It} came as a total surprise}'.

\ex. He lost his job. \underline{It} came as a total surprise.

We propose the identification of the three usage types of \textit{it}, namely anaphoric, event reference, and pleonastic, with a single system. We present several classification experiments which rely on information from the current and previous sentences, as well as on the output of external tools.


\section{Related Work}

Due to its difficulty, proposals for the identification and the subsequent resolution of abstract anaphora (i.e., event reference) are scarce \cite{Eckert2000,Byron2002,Navarretta2004,Muller2007}. The automatic detection of instances of pleonastic `it', on the other hand, has been addressed by the non-referential `it' detector NADA \cite{Bergsma2011}, and also in the context of several coreference resolution systems, including the Stanford sieve-based coreference resolution system \cite{Lee2011}.

The coreference resolution task focuses on the resolution of nominal anaphoric pronouns, de facto grouping our event and pleonastic categories together and discarding both of them. The coreference resolution task can be seen as a two-step problem: \textit{mention} identification followed by \textit{antecedent} identification. Identifying instances of pleonastic `it' typically takes place in the mention identification step. The recognition of event reference `it' is, however, to our knowledge not currently included in any such systems, although from a linguistic point of view, event instances are also referential \cite{Boyd2005}. As suggested by Lee et al.,~\shortcite{Lee2016},  it would be advantageous to incorporate event reference resolution in the second step.

In the context of machine translation, work by Le Nagard and Koehn~\shortcite{Lenagard2010}; Nov\'{a}k et al.~\shortcite{Novak2013}; Guillou~\shortcite{Guillou2015} and \newcite{Loaiciga2016WMT} have also considered disambiguating the function of the pronoun `it' in the interest of improving pronoun translation into different languages.  


\section{Disambiguating `it'}\label{sec:MaxEnt}

\subsection{Labeled Data}\label{subsection:golddata}

The ParCor corpus \cite{Guillou2014} and \textit{DiscoMT2015.test} dataset
\cite{DiscoMT2015TestSet} were used as gold-standard data. Under the ParCor annotation scheme, which was used to annotate both corpora, pronouns are manually labeled according to their function: anaphoric, event reference, pleonastic, etc. For all instances of `it' in the corpora, we extracted the sentence-internal position of the pronoun, the sentence itself, and the two previous sentences. All examples were shuffled before the corpus was divided, ensuring a balanced distribution of the classes (Table \ref{Tab:ittypesannotatedcorpus}). 

The pronouns `this' and `that', when used as event reference pronouns, may often be used interchangeably with the pronoun `it' \cite{Guillou2016Thesis}. We therefore automatically substituted all instances of event reference `this' and `that' with `it' to increase the number of training examples. 



\begin{table}[h]\centering
\small{
\begin{tabular}{|l||rrr|r|}
\hline
%Data			&\textit{it-}     &  &  & \\
Data set				&Event&Anaphoric&Pleonastic&Total\\
\hline\hline
\small{Training}&504&779&221&1,504\\
\small{Dev}		&157&252&92&501\\
\small{Test}	&169&270&62&501\\
\hline 
Total			&830&1,301  &375 &2,506\\
\hline 
\end{tabular}
}
\caption{Distribution of classes in the data.}\label{Tab:ittypesannotatedcorpus}
\end{table}


\subsection{Baselines}

We provide two different baselines (\textsc{MC} and \textsc{LM Baseline} in Table \ref{Tab:AllResults}). The first is a setting in which all instances are assigned to the majority class \textit{it-anaphoric}. The second baseline system is a 3-gram language model built using KenLM \cite{Heafield2011} and trained on a modified version of the annotated corpus in which every instance of `it' is concatenated with its function (e.g. `it-event'). At test time, the `it' position is filled with each of the three it-function labels in turn, the language model is queried, and the highest scoring option is chosen.


%We provide two different baselines (Table \ref{Tab:AllResults}). One is a setting in which all instances of the test set are assigned to the majority class \textit{it-anaphoric}. The second baseline is a 3-gram language model built using KenLM \cite{Heafield2011} and trained on a modified version of the annotated corpus in which every instance of `it' is concatenated with its function (e.g. `it-event'). At test time, the `it' position is filled with each of the three it-function labels and the language model is queried, choosing the highest scoring option.


% using 14-fold cross-validation and the single held-out test set. The motivation for 
%the choice of the number of folds is threefold. First, we wanted to respect
%document boundaries; second, we aimed for a fair proportion of the three classes in all folds; 
%and, lastly, we tried to lessen the variance given the relatively small size of the corpus. 

 
% \begin{table}[h]\centering
% \small{
%\begin{tabular}{|l||rrr|}
%
%%&\multicolumn{3}{c|}{Test-set majority class}\\
%\hline
%	\textbf{Majority class}&Precision&Recall&F1\\
%\hline\hline					
%\textit{it-} anaphoric	&0.539	&1&0.700\\
%\hline
%%&\multicolumn{3}{c|}{14-fold cross-validation}\\
%\hline
%\textbf{Dev-set}&Precision&Recall&F1\\
%\hline\hline
%\textit{it-} anaphoric	&	&&\\
%\textit{it-} pleonastic &	&&\\		
%\textit{it-} event		& &&\\	
%\hline
%%&\multicolumn{3}{c|}{Test-set}\\
%\hline
%\textbf{Test-set}	&Precision&Recall&F1\\
%\hline\hline
%\textit{it-} anaphoric	&0.732	&0.263&0.387\\
%\textit{it-} pleonastic &0.139	&0.694&0.231\\		
%\textit{it-} event		&0.521 &0.290&0.373\\
%\hline
%\end{tabular}\caption{`it' classification baselines.}\label{Tab:ClassificationBaselines}
%}
%\end{table}
% 


\subsection{Features}

%"Instead of assuming that all levels of abstract objects [event, concept, fact, proposition] are introduced to the discourse model by the clause that makes them available, ..." \cite{Eckert2000} 

We designed features to capture not only the token context, but also the syntactic and semantic context preceding the pronouns and, where appropriate, their antecedents/referents, as well as the pronoun head. We used the output of the POS tagger and dependency parser of \newcite{Bohnet2013}\footnote{We used the pre-trained models for English that are
available online {\url{https://code.google.com/p/mate-tools/downloads/list}}}, and of the TreeTagger lemmatizer \cite{Schmid1994} to extract the following information for each training example:

\paragraph{Token context (tok)}

\begin{enumerate*}[noitemsep,topsep=0pt,font={\color{red!50!black}\bfseries}]
\item Previous three tokens and next two tokens. This includes words, punctuation and the tokens in the previous sentence when the `it' occupies the first position of the current sentence.
\item Lemmas of the next two tokens.
\end{enumerate*} 

\paragraph{Pronoun head (hea)}

\begin{enumerate*}[resume,noitemsep,topsep=0pt,font={\color{red!50!black}\bfseries}]
\item Head word and its lemma. Most of the time the head word is a verb.
\item If the head verb is copular, we include its complement head and not the verb itself (for the verbs \textit{be}, \textit{appear}, \textit{seem}, \textit{look}, \textit{sound}, \textit{smell}, \textit{taste}, \textit{feel}, \textit{become} and \textit{get}).
\item Whether the head word takes a `that' complement (verbs only).
\item Tense of head word (verbs only), computed as described by \newcite{Loaiciga2014}.
\end{enumerate*} 

\paragraph{Syntactic context (syn)}

\begin{enumerate*}[resume,noitemsep,topsep=0pt,font={\color{red!50!black}\bfseries}]
\item Whether a `that' complement appears in the previous sentence. 
\item Closest NP head to the left and to the right.
\item Presence or absence of extraposed sentential subjects as in `\textit{So \underline{it}'s difficult \underline{to attack malaria} from inside malarious societies, [...]}.
\item Closest adjective to the right.
\end{enumerate*} 

\paragraph{Semantic context (sem)}

\begin{enumerate*}[resume,noitemsep,topsep=0pt,font={\color{red!50!black}\bfseries}]
\item VerbNet selectional restrictions of the verb. VerbNet \cite{Kipper2008} specifies 36 types of argument that verbs can take.
We limited ourselves to the values of \textit{abstract}, \textit{concrete} and \textit{unknown}.  %SL: I mean unknown for all the others
\item Likelihood of head word taking an event subject (verbs only). An estimate of the likelihood of a verb taking a event subject
was computed over the Annotated English Gigaword v.5 corpus
\cite{Napoles2012}. We considered two cases favouring event subjects that may be identified by
exploiting the parse annotation of the Gigaword corpus. The first case is when the subject is a gerund and the second case is
composed of `this' pronoun subjects. 
\item Non-referential probability assigned to the instance of `it' by NADA \cite{Bergsma2011}.
\end{enumerate*} 

\begin{figure}[h!]
\includegraphics[scale=0.27]{Feat_ablation_all} 
\caption{Feature ablation -- \textsc{MaxEnt} system.}\label{fig:featablation}
\end{figure}

%scale=0.27


\begin{table*}[t]\centering
\resizebox{\linewidth}{!}{ 
\begin{tabular}{|l||rrrr||rrrr|}
\hline
&\multicolumn{4}{|c|}{\textbf{Dev-set}}&\multicolumn{4}{|c|}{\textbf{Test-set}}\\
		\hline\hline
\textsc{MC Baseline}&Precision&Recall&F1&Accuracy&Precision&Recall&F1&Accuracy\\
		\hline\hline
\textit{it-anaphoric}	& 0.539 	&1	&0.700	&(252/501)&0.503 &1	&0.669&(270/501)\\
	&  		&	&& 0.503&	&&&0.539\\
		\hline\hline
\textsc{LM Baseline}&Precision&Recall&F1&Accuracy&Precision&Recall&F1&Accuracy\\
		\hline\hline
\textit{it-anaphoric}	& 0.613 	&0.290	&0.394	&(166/501)&0.732	&0.263&0.387&(163/501)\\
\textit{it-pleonastic}	& 0.169 	&  0.523	& 0.255	& 0.331	 &0.139	&0.694&0.231& 0.325
\\
\textit{it-event}		& 0.459	& 0.287	&0.353	&    &0.521 &0.290&0.373& \\
\hline\hline
\textsc{MaxEnt}&Precision&Recall&F1&Accuracy&Precision&Recall&F1&Accuracy\\
		\hline\hline
\textit{it-anaphoric}	& 0.685 	&\textbf{0.758}	&\textbf{0.719}	&  (326/501)   & 0.716 & \textbf{0.756}&\textbf{0.735}& (344/501)\\
\textit{it-pleonastic}	& \textbf{0.884} 	&\textbf{0.543} 	& \textbf{0.633}	& 	0.651  & \textbf{0.750} & 0.726&\textbf{0.738}& 0.687\\
\textit{it-event}		& \textbf{0.545}	&0.541 	&	\textbf{0.543}&     & \textbf{0.564} & 0.521&0.542& \\
\hline\hline
\textsc{Rnn-Gold}&Precision&Recall&F1&Accuracy&Precision&Recall&F1&Accuracy\\
\hline\hline
\textit{it-anaphoric}	& 0.544  & 0.560	& 0.552	& (221/501)	& 0.595 & 0.659 &	0.626	&	(250/501)\\
\textit{it-pleonastic}	& 0.274 	& 0.217	& 0.242 	&0.441	& 0.177 & 0.177 &0.177		&0.499	\\
\textit{it-event}		& 0.355  &0.382 	& 0.368	&	& 0.436 & 0.361 &	0.394	&	 \\
\hline\hline
\textsc{Rnn-Silver}&Precision&Recall&F1&Accuracy&Precision&Recall&F1&Accuracy\\
\hline
\textit{it-anaphoric}	&0.661   &0.611 	& 0.635	&(286/501)	& 0.706 &0.552  &	0.620	&(286/501)	\\
\textit{it-pleonastic}	& 0.725 	& 0.402	& 0.517 	&	0.571&  0.542 &0.516  &	0.529	&	0.571\\
\textit{it-event}		& 0.438  & 0.605	& 0.508	&	& 	0.455&0.621 & 0.525& \\
\hline\hline
\textsc{Rnn-Combined}&Precision&Recall&F1&Accuracy&Precision&Recall&F1&Accuracy\\
\hline
\textit{it-anaphoric}	& \textbf{0.697}  & 0.492	& 0.577	&(280/501)	& \textbf{0.794} &0.530  &	0.636	&(315/501)	\\
\textit{it-pleonastic}	& 0.633 	& \textbf{0.543}	& 0.585 	&0.559	& 0.582 & \textbf{0.742} &	0.652	&	0.629\\
\textit{it-event}		& 0.434  & \textbf{0.675}	& 0.529	&	& 0.520 & \textbf{0.746} &\textbf{0.613}		&	 \\
\hline
\end{tabular}
}\caption{Comparison of baselines and classification results.} \label{Tab:AllResults}
\end{table*}




\subsection{MaxEnt}


The \textsc{MaxEnt} classifier is trained using the Stanford Maximum Entropy package \cite{Manning2003} with all of the features described above. We also experimented with other features and options. For features 1 and 2, a window of three tokens showed a degradation in performance. For feature 8, adding one of the 26 WordNet \cite{wordnet} types of nouns had no effect. The feature combination of noun and adjectives to the left or right also had no effect. Feature ablation tests revealed that while combining all features is beneficial for the prediction of the anaphoric and pleonastic classes, the same is not true for the event class. In particular, the inclusion of semantic features, which we designed as indicators of event\textit{ness}, appears to be harmful (Figure \ref{fig:featablation}).  





\subsection{Unlabeled Data}

Given the small size of the gold-standard data, and with the aim of gaining insight from unstructured and unseen data, we used the \textsc{MaxEnt} classifier to label additional data from the pronoun prediction shared task at WMT16 \cite{Guillou2016WMT}. This new \textit{silver-standard} training corpus comprises 1,101,922 sentences taken from the Europarl (3,752,440 sentences), News (344,805 sentences) and TED talks (380,072 sentences) sections of the shared task training data.

\begin{table*}[h!]
\resizebox{\linewidth}{!}{ 
\begin{tabular}{|p{10.5cm}|r|r|}
\hline
\textsc{Reference relationship}&\textsc{MaxEnt}&\textsc{Rnn-Combined}\\
\hline
\hline
(1) NP antecedent in previous 2 sentences  & \textbf{*(191/248)}& (136/248) \\
\small{\textit{e.g. The infectious disease that's killed more humans than any other is \textbf{malaria}. It's carried in the bites of infected mosquitos, and \textbf{it}'s probably our oldest scourge.}}& \textbf{0.770} &0.548\\
\hline
(2) VP antecedent in previous 2 sentences & (25/38) & \textbf{(27/38)} \\
\small{\textit{e.g. And there's hope in this next section, of this brain section of somebody else with M.S., because what it illustrates is, amazingly, the brain can \textbf{repair itself}. It just doesn't do \textbf{it} well enough.}}&0.658&\textbf{0.711}\\
\hline
(3) NP or VP antecedent further away in the text (not in snippet) &(28/47) & (28/47) \\
\small{\textit{e.g. It has spread. It has more ways to evade attack than we know. \textbf{It}'s a shape-shifter, for one thing.}}&0.596&0.596\\
\hline
(4) Sentential or clausal antecedent &(52/88)& \textbf{*(66/88)} \\
\small{e.g. \textit{\textbf{Pension systems have a hugely important economic and social role and are affected by a great variety of factors.} \textbf{It} has been reflected in EU policy on pensions, which has become increasingly comprehensive over the years.}} &0.591&\textbf{0.750}\\
\hline
(5) Pleonastic constructions &(43/59) & (42/59) \\
\small{\textit{e.g. And \textbf{it} seemed to me that there were three levels of acceptance that needed to take place.}}&0.729&0.728\\
\hline 
(6) Ambiguous between event and anaphoric &(3/12)& \textbf{(7/12)} \\
\small{\textit{e.g. Today, multimedia is a desktop or living room experience, because the apparatus is so clunky . \textbf{It} will change dramatically with small, bright, thin, high-resolution displays.}}& 0.250&\textbf{0.583}\\
\hline
(7) Ambiguous between event and pleonastic & \textbf{(2/5)} & (1/5) \\
\small{\textit{e.g. I did some research on how much it cost, and I just became a bit obsessed with transportation systems. And \textbf{it} began the idea of an automated car.}} &\textbf{0.400}&0.200\\
\hline
(8) Annotation errors &(0/4)&(0/4)\\
\small{\textit{e.g. Youth unemployment is particularly worrying in \textbf{it} context, as the lost opportunity for jobless young people to develop professional skills is likely to translate into lower productivity and lower earnings over a longer period of time.}}&--&--\\
\hline
\end{tabular}
}
\caption{Accuracy scores of the systems in different portions of the test-set. For each category, we test whether \textsc{MaxEnt} is better or worse than \textsc{RNN-Combined}. A * indicates significance at $p<0.001$ using McNemar's $\chi^{2}$ test.}\label{tab:erroranalysis}
\end{table*}


\subsection{RNN}


Our second system is a bidirectional recurrent neural network (RNN) which reads the context words and then makes a decision based on the representations that it builds. Concretely, it consists on word-level embeddings of size 90, two layers of Gated Recurrent Units (GRUs) of size 90 as well, and a final softmax layer to make the predictions. The network uses a context window of 50 tokens both to the left and right of the `it' to be predicted. The features described above are also fed to the network in the form of one-hot vectors. The system uses the \textit{adam} optimizer and the categorical cross-entropy loss function. We chose this architecture following the example of Luotolahti et al.~\shortcite{Luotolahti2016}, who built a system for the related task of cross-lingual pronoun prediction. 

%RNN models may be used to capture long range dependencies. Assuming that pronouns are part of a bigger coreference chain, capturing other pronouns and mentions in the same chain, may influence the choice of function label assigned to the pronoun of interest. 


\section{Discussion}


We report all of the results in Table \ref{Tab:AllResults}. \textsc{MaxEnt} and \textsc{Rnn-Gold} are trained on the gold-standard data only. \textsc{Rnn-Silver} is trained on the silver-standard data (annotated using the \textsc{MaxEnt} classifier). \textsc{Rnn-Combined} is trained on both the silver-standard and gold-standard data. 

The \textsc{MaxEnt} and \textsc{Rnn} models show improvements, albeit small for the it-event class, over the baseline systems. 
Since they are trained on the same gold-standard data, one would expect \textsc{Rnn-Gold} to perform similarly to \textsc{MaxEnt}. However, in the case of the RNN-gold, the 50 tokens window may actually not have enough words to be filled with, because the gold-standard data is composed of the sentence with the it-pronoun and the three previous sentences, which in addition tend to be short. For the \textsc{Rnn-Silver} system this is not a problem, since the sentences of interest have not been taken out of their original context, fully exploiting the \textsc{Rnn} capacity to learn the entirety of the context window they are presented with, even if the data is noisy. As expected, \textsc{Rnn-Combined} performs better than \textsc{Rnn-Gold} and \textsc{Rnn-Silver}. Although it does not perform overwhelmingly better than \textsc{MaxEnt}, there are gains in precision for the it-anaphoric class, and in recall for the it-pleonastic and it-event classes, suggesting that the system benefits from the inclusion of gold-standard data. 


With the two-fold goal of gaining a better understanding of the difficulties of the task and strengths of the systems, we re-classified the test set in a stratified manner. We present the systems with seven scenarios reflecting the different types of reference relationships observed in the corpora (Table \ref{tab:erroranalysis}). Our scenarios are exhaustive, thus some only have few examples. The analysis reveals that the \textsc{MaxEnt} is a better choice for nominal reference (case (1), mostly \textit{it-anaphoric}) whereas the \textsc{Rnn-Combined} system is better at identifying difficult antecedents such as cases (4) and (6). \textsc{Rnn-Combined} performs slightly better at detecting verbal antecedents, case (2), while both systems perform similarly at learning pleonastic instances (5) or when the antecedent is not in the snippet (3). Finally, we found 4 instances of annotation errors (8). These correspond to some of the automatically substituted cases of `this'/`that' with `it', for which the `this'/`that' should not have been marked as a pronoun by the human annotator in the first place. Case (8) is not taken into account in the evaluation.

Taking the complete test set, we found that the \textsc{MaxEnt} system performs better than the \textsc{Rnn-Combined} system in absolute terms ($\chi^{2} = 50.8891, p<0.001$), but this is because case (1) is the most frequent one, which is also the case the \textsc{MaxEnt} system is strongest at.



%Our scenarios are exhaustive, with the outcome that some have only few examples.
 
%With the two-fold goal of gaining a better understanding of the difficulties of the task and the strengths of the systems, we re-classified the test set in a stratified manner. We present the systems with seven groups of sentences reflecting the different types of reference relationships observed in the corpus (Table \ref{tab:erroranalysis}). This analysis showed that the \textsc{MaxEnt} is a better choice for the nominal reference (case (1), mostly \textit{it-anaphoric}) but the \textsc{Rnn-Combined} system shines at the identification of difficult antecedents, cases (4) and (6). \textsc{Rnn-Combined} seems also slightly better for detecting verbal antecedents (case (2)), while both systems perform similarly at learning pleonastic instances (5) or when the antecedent is not in the snippet (3).

%An it-event pronoun could be seen as a referential pronoun lacking a nominal antecedent. In this sense, the \textsc{MaxEnt} classifier identifies more antecedents than there actually are, while the \textsc{Rnn-Combined} fails to recognize them.







%%SL: there were 4 errors ("that"s which shouldn't have been marked as pronouns in the first place). I didn't include them in the table but I have the numbers. 

%% exact significance results: $\chiˆ2$ = 20.0792, df = 1, p-value = 7.43e-06 for case (1), and $\chiˆ2$ = 11.3649, df = 1, p-value = 0.0007485 for case (4)





%\textcolor{blue}{Manual inspection of the results confirmed that \textsc{MaxEnt} makes fewer errors in identifying the it-anaphoric class when there is a clear antecedent nearby (49/118 vs. 96/118 errors). For example, `\textit{We trust that they will spend their time with us, that they will play by the same rules, value the same goal, they'll stay with \underline{the game} until \underline{it}'s over.}'. \textsc{Rnn-Combined}, on the other hand, performs better for the it-event class, particularly in difficult cases where the reader must infer the event from the text (12/25 vs. 22/25 errors). E.g. in `\textit{According to the same survey, 61\% of the population aged 15 and over believe that people should be able to continue to work beyond any statutory retirement age: in other words, they reject the idea of a mandatory retirement age. \underline{It} is an important and welcome development.}'} 

%\textcolor{blue}{The inter-annotator agreement kappa score of 0.81 for the gold corpus \cite{Guillou2016Thesis}, represents a reasonable upper bound for system performance. \textcolor{blue}{rather report accuracy or raw agreement, so it's comparable}
%18 of the 27 disagreements arose due to differences in opinion as to whether an instance of `it' is anaphoric or event reference. In our own evaluation, we counted 35 such ambiguous cases, and found that \textsc{Rnn-Combined} yields marginally fewer errors than \textsc{MaxEnt} (23 vs 27). }

%\cite[p.81]{Guillou2016Thesis}

%\textcolor{blue}{Examining these ambiguous cases more closely, we looked at the intersection of errors between \textsc{MaxEnt} and \textsc{Rnn-Combined}, focusing on the cases where both systems disagreed with the gold labels. \textcolor{blue}{We found that in 11 of 72 cases, the systems prediction was better than the gold label}, pointing to a possible 15\% error rate in the gold-standard annotation.} 

%\textcolor{blue}{Lastly, it-pleonastic is easily identified by \textsc{MaxEnt} and not \textsc{Rnn-Combined}. Typical pleonastic constructions such as \textit{`\underline{It} is crucial to create opportunities so that employment rates of people above age 55 continue to increase.'} are systematically incorrectly classified as it-event by the \textsc{Rnn-Combined} system.}


\section{Conclusions and Future Work}
\label{sec:conclusions}

We have shown that distinguishing between nominal anaphoric and event reference realizations of `it' is a complex task. Our results are promising, but there is room for improvement. The self-training experiment demonstrated the benefit of combining gold-standard and silver-standard data. 

We also found that the \textsc{Rnn-Combined} system is better at handling difficult and ambiguous referring relationships, while the \textsc{MaxEnt} performed better for the nominal anaphoric case, when the antecedent is close. Since the two models have different strengths, in future work we plan to enrich the training data with re-training instances from the silver data where the two systems agree, in order to reduce the amount of noise, following the example of Jiang et al.~\shortcite{Jiang2016}. 

Ultimately, we aim towards integrating the it-prediction system within a full machine translation pipeline and a coreference resolution system. In the first case, the different translations of pronoun `it' can be constrained according to their function. In the second case, the performance of a coreference resolution system vs a modified version using the three-way distinction can be measured.





\section*{Acknowledgments}

This work was supported by the Swedish Research Council under project 2012-916 \emph{Discourse-Oriented Statistical Machine Translation}. We used computing resources on the Abel cluster, owned by the University of Oslo and the Norwegian metacenter for High Performance Computing (NOTUR) and on the Taito-CSC cluster provided through the Nordic Language Processing Laboratory (NLPL) (\url{www.nlpl.eu}). SL was also supported by the Swiss National Science Foundation under grant no.~P1GEP1\_161877.


%The acknowledgments should go immediately before the references.  Do
%not number the acknowledgments section. Do not include this section
%when submitting your paper for review.




\bibliography{biblio}
\bibliographystyle{emnlp_natbib}


\end{document}
